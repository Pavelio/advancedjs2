class HamburgerException extends Error {
    constructor(msg) {
        super(msg);
        this.error = "Hamburger Exception";
        this.message = msg;
    }  
}

class Hamburger {
    constructor(size, stuffing) {
        try {
            if (typeof (size) !== "object" || typeof (stuffing) !== "object") {
                throw new HamburgerException("Hamburger arguments must be an object");
            }
            if (!size || !stuffing) {
                throw new HamburgerException("No argument");
            }
            if (size.name !== "SIZE_SMALL" && size.name !== "SIZE_LARGE") {
                throw new HamburgerException("The first argument is size. Enter size");
            }
            if (stuffing.name !== "CHEESE" && stuffing.name !== "SALAD" && stuffing.name !== "POTATO") {
                throw new HamburgerException("The second argument is stuffing. Enter stuffing");
            }
            this._size = size;
            this._stuffing = stuffing;
        }
        catch (HamburgerException) {
            return HamburgerException.message;
        } 
        finally {
            this._topping = [];
        }
    }

    get size() {
        return this._size.name;
    };

    get stuffing() {
        return this._stuffing.name;
    };

    set addTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException("Topping not entered");
            }
            if (typeof (topping) !== "object") {
                throw new HamburgerException("Hamburger topping - has wrong data type. Еnter object");
            }
            this._topping.forEach(function (elem) {
                if (topping.name === elem.name) {
                    throw new HamburgerException("Duplicate topping. Сan only one");
                }
            });
            this._topping.push(topping);
        }
        catch (HamburgerException) {
            return HamburgerException.message;
        }
    };

    removeTopping(removeTopping) {
        try {
            if (!removeTopping) {
                throw new HamburgerException("Topping not entered");
            }
            this._topping.some(function (elem) {
                return removeTopping.name === elem.name;
            });
            const result = this._topping.filter(elem => elem.name !== removeTopping.name);
            this._topping = result;
        }
        catch (HamburgerException) {
            return HamburgerException.message;
        }
    };

    get Toppings() {
        const arrTopping = [];
        this._topping.forEach(function (elem) {
            arrTopping.push(elem.name);
        });
        return arrTopping;
    };

    calculatePrice() {
        const calcToppingPrice = this._topping.reduce(function (sum, current) {
            return sum += current.price;
        }, 0);
        const priceCalc = this._size.price + this._stuffing.price + calcToppingPrice;
        return priceCalc;
    };
    
    calculateCalories() {
        const calcToppingCalories = this._topping.reduce(function (sum, current) {
            return sum += current.calories;
        }, 0);
        const caloriesCalc = this._size.calories + this._stuffing.calories + calcToppingCalories;
        return caloriesCalc;
    };
}

Hamburger.SIZE_SMALL = { name: "SIZE_SMALL", price: 50, calories: 20 };
Hamburger.SIZE_LARGE = { name: "SIZE_LARGE", price: 100, calories: 40 };
Hamburger.STUFFING_CHEESE = { name: "CHEESE", price: 10, calories: 20 };
Hamburger.STUFFING_SALAD = { name: "SALAD", price: 20, calories: 5 };
Hamburger.STUFFING_POTATO = { name: "POTATO", price: 15, calories: 10 };
Hamburger.TOPPING_MAYO = { name: "TOPPING_MAYO", price: 20, calories: 5 };
Hamburger.TOPPING_SPICE = { name: "TOPPING_SPICE", price: 15, calories: 0 };

const hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);
hamburger.addTopping = Hamburger.TOPPING_MAYO;
hamburger.addTopping = Hamburger.TOPPING_SPICE;
console.log(`Hamburger topping: ${hamburger.Toppings}`);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`Hamburger topping: ${hamburger.Toppings}`);
console.log(`Hamburger price: ${hamburger.calculatePrice()}UAH`);
console.log(`Hamburger calories: ${hamburger.calculateCalories()}`);
console.log(`Hamburger size: ${hamburger.size}`);
console.log(`Hamburger stuffing: ${hamburger.stuffing}`);